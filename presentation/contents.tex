\section{Introduction}

\begin{frame}{In this presentation}
  \begin{itemize}
    \item What is transactional memory?
    \item Different types of TM
    \item Details on these types
    \item Where is it used today
    \item Outlook \& Conclusion
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{What is transactional memory?}

\begin{frame}{Rise of parallel programs}
  \begin{itemize}
    \item Rise of multi-core processors
    \item In order to leverage performance: \\
          Shift from sequential to parallel programs
    \item Communication through shared memory
  \end{itemize}
\end{frame}

\begin{frame}{Today's synchronization mechanisms}
  \begin{itemize}
    \item Explicit synchronization
    \item Low-level locks and mutexes
    \item Tedious to write \& error-prone
    \item Hard to debug \& maintain
  \end{itemize}
\end{frame}

\begin{frame}{The concept of transactional memory}
  \begin{itemize}
    \item Fundamental idea: \\
          \textbf{Say \emph{what} to synchronize, not \emph{how}}
    \item Concepts from database community
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item Shared memory accessed in a \emph{transaction}
        \item \emph{ACID} properties: Implement \emph{atomicity} and \emph{isolation}
      \end{itemize}
    \item \emph{Commit} if successful, \emph{rollback} if not
    \item Goal: Make parallel programming easier
    \item Features: e.g. dead- and livelock detection, \emph{composability}
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Different types of TM}

\begin{frame}{\currentname}
  \begin{itemize}
    \item Systems:
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item \textbf{Software TM}
        \item \textbf{Hardware-accelerated Software TM}
        \item \textbf{Hardware TM}
      \end{itemize}
    \item Main differences:
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item Approach
        \item Granularity
        \item Update policy
        \item Conflict detection policy
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Approach}
  \begin{itemize}
    \item \textbf{Optimistic}: Generally assume that it will not conflict
    \item Most implementations choose this
    \item In contrast to pessimistic: \\
      Do not establish exclusive access before accessing shared data

    \item \textbf{Pessimistic}: Difference between normal locks: \\
      Basically, ability to abort at e.g. deadlock
  \end{itemize}
\end{frame}

\begin{frame}{Granularity}
  \begin{itemize}
    \item How fine-grained are conflicts detected?
    \item \textbf{Object-}, \textbf{cache-line-} or \textbf{word-level}
    \item Balance false positive detection and system complexity
  \end{itemize}
\end{frame}

\begin{frame}{Update policy}
  \begin{itemize}
    \item A \textbf{direct update} system makes changes in-place
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item Need an ``undo log'' in case of a conflict
        \item Can become tricky: e.g. I/O operation
      \end{itemize}
    \item A \textbf{deferred update} system copies the shared memory
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item Work on private copy only
        \item Simplicity at the price of an overhead
      \end{itemize}
    \item Data isolation: \textbf{strong vs weak atomicity}
  \end{itemize}
\end{frame}

\begin{frame}{Conflict detection policy}
  \begin{itemize}
    \item Conflict detection can either be \textbf{early} or \textbf{late}
    \item \textbf{Early} detects conflict upon first access to object
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item Can prevent unnecessary computation
      \end{itemize}
    \item \textbf{Late} detects when trying to commit
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item Can prevent unnecessary transaction aborts
      \end{itemize}
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Details on Software TM}

\begin{frame}{\currentname}
  \begin{itemize}
    \item It is indeed possible to implement a TM without locks!
    \item Advantage: Hardware-independent
    \item Disadvantage: Slow performance and high overhead
    \item May outperform locks when there is a large number of CPUs
    \item Generally 3 -- 5 times slower than sequential code
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Details on Hardware-accelerated STM}

\begin{frame}{\currentname}
  \begin{itemize}
    \item Overhead of STM: mainly maintenance \& validation of read sets
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item Read set: List of objects from which transaction has read the value
        \item Therefore: Make hardware that helps with bookkeeping
      \end{itemize}
    \item Use \emph{mark bits} to keep track of memory access
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item Can create fast filters using those bits
      \end{itemize}
    \item Goal: Speed up common TM system operations
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item Up to twice as fast as STM
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Example: HASTM}
  \begin{itemize}
    \item Per-thread mark bits at cache-line granularity
    \item If shared memory read is done:
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item HW checks if mark bit for this thread is already set
        \item If not: Add memory address to read set
      \end{itemize}
    \item Upon validation:
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item Ask hardware if marked cache block has been invalidated
        \item Hardware knows this because it monitors cache coherence messages
        \item Invalidation \emph{could} mean that another thread changed data
        \item But: Can also be due to cache capacity, interrupt, context switch, ...
        \item In this case: Fall back to software validation
      \end{itemize}
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Details on Hardware TM}

\begin{frame}{\currentname}
  \begin{itemize}
    \item Does not \emph{necessarily} need software instrumentation
    \item Can manage conflicts transparently
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item Entire transactional bookkeeping, validation, etc in hardware
        \item Programs just perform ordinary read/write operations
      \end{itemize}
    \item Drawback: Needs special hardware \ra not portable
    \item Make use of cache hierarchy and cache coherence protocol
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item Caches observe all reads and writes issued by a CPU
        \item Again: Use \emph{mark bits}
      \end{itemize}
    \item Excellent performance
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item 2 -- 10 percent slower than sequential code
        \item Up to 4 times faster than classic locks
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Example: TCC}
  \begin{itemize}
    \item Deferred update HTM
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item Cache blocks in \emph{write} set do not propagate changes until commit
      \end{itemize}
    \item Upon commit:
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item Use cache coherence protocol to get exclusive access to all cache blocks
        \item If success: reset all mark bits
        \item This triggers cache coherence propagation
        \item Bonus: Multiple transactions can commit in parallel
      \end{itemize}
    \item For this system: Conflicts trigger software handler
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{TM today}

\begin{frame}{\currentname}
  \begin{itemize}
    \item Still a research topic
    \item Already integrated in \emph{Haskell} and \emph{Java}
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item Java: The \texttt{atomic} keyword
        \item Haskell: Preferred method of synchronization
      \end{itemize}
    \item Few processors have HTM
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item But: Will probably become common, see e.g. \emph{Intel TSX}
      \end{itemize}
    \item Language extensions for \emph{C} and \emph{C++} in \texttt{gcc 4.7 +}
    \item Research-quality extensions / libraries for many other languages
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Example: GCC 4.7 or newer}
  \begin{verbatim}
  ...
  __transaction_atomic {
      counter ++;
  }
  ...
  \end{verbatim}

  Compile with \texttt{-fgnu-tm}
\end{frame}

\begin{frame}{Performance of GCC TM}
  \begin{figure}[tbp]
    \begin{center}
      \resizebox{!}{0.7\textheight}{\input{benchmark_results.tex}}
      \caption{Benchmark results for $n = 1000000$ with 4 threads}
      \label{fig:benchmark}
    \end{center}
  \end{figure}
\end{frame}

\begin{frame}{Example: Intel TSX}
  \begin{itemize}
    \item \emph{Transactional Synchronization Extensions}
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item On selected Haswell chips and newer
      \end{itemize}
    \item Pure HTM
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item Optimistic, cache-line granularity, deferred update
        \item Conflict detection though cache coherence protocol
      \end{itemize}
    \item Introduces a couple of new assembly instructions
    \item Two different software interfaces:
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item \textbf{Hardware Lock Elision}, simpler but backwards compatible
        \item \textbf{Restricted TM}, allows for separate alternative code path
      \end{itemize}
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Outlook \& Conclusion}

\begin{frame}{\currentname}
  \begin{itemize}
    \item Need for simple and well-performing sync mechanisms
    \item Likely to become more popular in the next few years
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item There are already production-ready TM systems
        \item Permanent addition to X86 ISA
      \end{itemize}
    \item The goal is not to \emph{replace}, but to \emph{complement} classic locks
    \item But still an area of active research
      \begin{itemize}
        \setlength{\itemsep}{.3em}
        \item Still no well-defined semantics
        \item Lack of unified benchmarks \ra difficult to compare systems
        \item Implementation variants, e.g. should HTM fall back to STM for conflicts?
      \end{itemize}
    \item All in all: very promising future
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\plain{EOF}

