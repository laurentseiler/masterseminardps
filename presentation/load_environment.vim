set scrolloff=10  " always keep some lines visible

map  <F1> @q

map! <F4> \emph{}<Esc>ci{
map  <F4> di\emph{}<Esc>Pl

map! <F5> \textbf{}<Esc>ci{
map  <F5> di\textbf{}<Esc>Pl

map! <F6> \texttt{}<Esc>ci{
map  <F6> di\texttt{}<Esc>Pl

map! <F9> \begin{itemize}<CR>\item <CR>\end{itemize}<C-o>k
map! <S-F9> <F9><Esc>O\setlength{\itemsep}{.3em}<Esc>ja

map  <F10> a<F10>
map! <F10> \begin{frame}{}<C-o>mt<CR>\end{frame}<C-o>`t

map  <F11>   :w<CR>:!(make &) > /dev/null<CR><CR>

map! <C-i> \item<Space>

