set formatoptions+=a        " auto line break
set whichwrap+=<,>,h,l,[,]  " go to next line after line end
set scrolloff=10            " always keep some lines visible

map <F11>   :w<CR>:!(make std  &) > /dev/null<CR><CR>
map <S-F11> :w<CR>:!(make ieee &) > /dev/null<CR><CR>

map! <F4> \emph{}<C-o>h<C-o>a
map  <F4> di\emph{}<Esc>Pl
