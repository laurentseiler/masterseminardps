\begin{abstract}

  Transactional memory is a new concept to access shared memory. In contrast to
  classic lock-based systems, this ongoing but maturing research topic seeks to
  avoid global locking and borrows some ideas from the database community
  instead: Concurrent access should be secured by TM \emph{transactions}. The
  goal is to make parallel programming easier and clearer by using concise
  semantics that specify \emph{what} should be synchronized, but not \emph{how}
  -- a task that is no longer left to the programmer, but to the system. In
  this paper, our focus is to introduce transactional memory and it's
  principles to programmers that are new to the topic and try to give a general
  overview of the ideas and fundamental concepts of TM by covering a wide range
  of topics in the field, ranging from software- to hardware-based systems down
  to the practical usage of TM techniques in today's software, and conclude
  that TM is ready for production, albeit not mission-critical use and is
  likely to see widespread adoption in the years to come.

\end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Introduction}

In the past years, we have seen a shift from sequential programming towards
parallel programming. Most processors have multiple cores today. To leverage
their computation power, we need to write software in such a way that it is
actually able to distribute it's work over multiple cores. Communication
between threads is generally achieved by using shared memory. However, in
multi-threaded programs, we face challenges that do not appear in sequential
code. Access to shared memory needs to be coordinated. This is usually done
with locking mechanisms such as mutexes, where coordination turns out to be a
hard task. Such programs are error-prone, they can get stuck in deadlocks or
livelocks and may have race conditions on the shared data if there is not
enough synchronization. The programs become tedious to write, read, maintain
and debug, and the difficult reasoning often leads to badly written programs
that scale poorly. Today's locking techniques are low-level locks that make
explicit synchronization necessary and the programmer needs to orchestrate the
complex interactions between the threads. Finally, from a performance
perspective, as the number of processors grows, the cost of locking becomes
more and more important and can lead to a significant overhead.

Transactional memory aims to provide higher-level abstractions to express
parallel computation. The general idea is to say \emph{what} should be
synchronized, not \emph{how} \cite{scott2015transactional}. If synchronization
between threads becomes simpler, it is easier for a programmer to reason about
the correctness of the code as he does not need to worry about the complex
interactions between threads anymore. To achieve this goal, transactional
memory borrows it's ideas from the database community. DBMS generally have the
\emph{ACID} properties, atomicity, consistency, isolation and durability. The
\emph{A} and \emph{I} properties are also desirable for a transactional memory
system. They ensure that a command to a database is done in an atomic way,
meaning either everything or nothing is done, and in isolation from other
calls, as if the DBMS would execute sequential code -- a property known as
``\emph{serializability}'' \cite{larus2008transactional}. Equipped with these
two properties, the goal of the TM system is to shift the burden of
synchronization away from the programmer and towards the TM system
\cite{grossman2007transactional}. It should offer the ease-of-use of
coarse-grained locks while still offering most of the performance of
fine-grained locks \cite{scott2015transactional}.

TM is still a research topic, but it has become mature enough to be used in
today's software. It is still not widely used, but this is likely to change in
the years to come. On the one hand, the TM systems have become good enough for
production use, and on the other hand, the rise of multi-core processors makes
good parallelization necessary and the simplicity of TM makes it a highly
desirable candidate to achieve this goal. What is more, is that TM should
support \emph{composability}. This property describes the possibility of
combining multiple (TM-synchronized) building blocks without having to worry
about implementation details and represents a fundamental breakthrough in the
creation of concurrent abstractions \cite{scott2015transactional}.
% TODO: Example?

\subsection{Scope of this paper}

The aim of this paper is to provide an introduction to transactional memory and
to give an overview over the different systems and techniques that exist today.
It should present TM in a \emph{pragmatic fashion} for programmers that are new
to the topic, and convey the basic principles in such a way that the reader has
a good overview over the topic that is likely to enter the wider discourse in
the near feature.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Overview}

Transactional memory systems can come in three forms: Either \emph{software TM}
(STM), \emph{hardware TM} (HTM), or hardware-accelerated software TM. Although
the implementation can be significantly different, the basic idea is always the
same. In classical lock-based synchronization, a thread has to hold a lock
before accessing shared data in order to make sure he has exclusive access.  In
contrast, transactional memory does generally not need to hold such locks.  The
shared data can just be accessed and manipulated. If a conflict is detected,
the actions are undone. If the work is finished and no conflict has happened,
the changes are committed and the actions on the shared data have been
successful.  It varies from implementation to implementation how this process
works in detail. TM systems can be categorized by a number of properties, which
is what we explore in this section.

At first, an implementation can be \emph{optimistic} or \emph{pessimistic}. In
case of the former, an implementation generally assumes that it will not
conflict. This is what most implementations chose to implement. It has one
major advantage: In contrast to pessimistic TM, the optimistic systems do not
need to establish exclusive access to the shared resource. As mentioned above,
if a conflict is detected, the changes are rolled back. Pessimistic systems do
not face this kind of problem. They mainly differ from classic locks in their
ability to abort a transaction in case of e.g. a deadlock.

Second, TM system differ in their \emph{granularity}, which can be at
\emph{object-level}, \emph{word-level}, or \emph{cache line-level}. This
granularity has an impact on two aspects of the system: On the one hand, it
determines how fine-grained the conflict detection can work. Clearly, a more
fine-grained detection can prevent unnecessary aborts by avoiding false
positive conflict detections. We note that the converse does never happen:
there are no false negatives, meaning that \emph{if} a conflict happens, it
will always be detected. On the other hand, the granularity has an impact on
both the complexity of the TM implementation as well as it's bookkeeping
overhead.  Coarse granularity clearly generates less overhead and makes the
implementation simpler. However, surveying the literature, we get the
impression that no method performs consistently better than the other. There
are many factors to be taken into account, the actual code that is run using
the TM system being one of them.

Third, TM systems can have \emph{direct-update} or \emph{deferred-update}
policies. This makes a huge difference in the system's design. A
deferred-update system makes a private copy of a TM object before modifying it,
and then continues it's work on this private memory only
\cite{blundell2006subtleties}\cite{adl2006compiler}. The changes are only made
visible to other threads when this transaction commits.  Although this approach
may have a considerable overhead, it can simplify many aspects of lock-free
parallel execution. The most obvious advantage is that a rollback can be done
by simply discarding the private copy of the object. The other advantage
concerns the interplay between the threads. Imagine that two parallel TM
operations are carried out on the same shared memory and both have write
operations in them.  This operation is doomed and at least one TM operation
needs to abort. In case of private copies, it will be sufficient to abort one
operation, whereas a direct-update system would need to rollback both, as you
cannot be sure that no intermediate state from one transaction has leaked into
the other one.  Moreover, note that the complete access to shared data needs to
be TM-aware in direct-update systems \cite{shpeisman2007enforcing}. Otherwise,
it would be possible for non-transactional code to read data from an
intermediate state of a transaction and it would be impossible for the TM
system to notice. The (non-)isolation of transactional from non-transactional
code is also known as \emph{strong} and \emph{weak atomicity}. In general, STM
systems implement weak atomicity, whereas HTM systems implement strong
atomicity, which gives rise to the challenge of coping with the semantic
differences that arise, in order to design a unified interface for the
programmer. In contrast to deferred-update systems, direct-update systems need
some kind of ``undo log'' which temporarily saves the original values of
objects before they are modified for the case where the transaction fails and
the original values need to be restored.  However, the restoring of the
original state can become pretty tricky.  Consider for example I/O operations
and a print operation inside a transaction.  If this transaction fails, it is
impossible to undo the side effects, hereby violating the atomicity axiom.
This particular case can be circumvented by placing output data into a buffer
that is flushed when the transaction has committed successfully, but
nevertheless shows that special care needs to be taken for certain calls.

The fourth major difference in TM systems is it's \emph{detection policy},
which can be either \emph{early} or \emph{late conflict detection}. Early
detection finds a conflict upon the first access to an object, whereas late
detection finds a conflict when a transaction tries to commit. As always, every
method has it's advantages and disadvantages. Early detection can prevent
unnecessary computation of a doomed transaction that will be rolled back, but
may induce unnecessary aborts, e.g. when the conflicting transaction itself is
discarded later due to another conflict with a third transaction.
Unfortunately, no method is consistently superior
\cite{larus2008transactional}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Software TM}

It is possible to implement a lock-free TM system only in software, i.e.
without support of specialized hardware. A notable example is the \emph{WSTM}
system by Harris and Fraser. It is a direct-update system with word granularity
conflict detection and it is widely used: It is the TM behind the
\texttt{atomic} keyword in the Java programming language.

The obvious advantage of STM systems is that it does not need special hardware
to run and hence is a good choice to support a large number of platforms.
Unluckily, this comes at a high cost: Generally, the overhead of these systems
is so large that it performs worse than old-fashioned locks when deployed on a
computer with a small number of processors. The performance depends largely on
the code that should be executed, e.g. how frequent read- or write operations
are, and how many conflicts arise, so no clear line can be drawn here as to
when which synchronization mechanism performs better. We can, however, state
that generally, as the number of processors becomes larger and therefore the
cost of locking increases, STM systems have a fair chance to outperform classic
lock-based code \cite{larus2008transactional}. Depending on the sources, STM
code runs 2 to 7 times \cite{minh2007effective} or 3 to 5 times
\cite{scott2015transactional} slower than sequential code.

Another topic that should be mentioned here is how a TM system decides which
transaction should be aborted at a conflict. Although no policy could be found
that constantly performs better than every other policy, there is a policy
called ``Polka'' that yielded generally good performance
\cite{guerraoui2005polymorphic}\cite{scherer2005advanced}. Transactions get a
priority based on how many objects they have in use, and a higher-priority
transaction aborts a conflicting lower-priority one. An conflicting transaction
tries to re-execute $n$ times after an exponentially increasing time interval,
where $n$ is the difference of their priorities. If the transaction fails after
$n$ attempts, control is given back to the TM system, which may re-execute it
or even choose an alternative, e.g.  lock-based code path.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Hardware-accelerated STM}

A large part of the overhead of STM systems comes from the maintenance and
validation of read sets \footnotemark. Specialized hardware which helps with
these tasks can alleviate the performance hit and make an STM up to twice as
fast.

\footnotetext{Clarification: The read set is a list of objects from which a
transaction has read the value}

An STM system has the following general workflow: For every shared memory read,
an internal routine is invoked which adds the respective memory address to the
read set. In early-detection systems, it may also already check for conflicts
by examining the internal version number of the data or it's locks. Then, when
the transaction is ready to commit, the STM system goes through the read set
and checks if there are any conflicts using the internal version number of the
data.

If there are lots of TM invocations on small data sets, this procedure
introduces a comparatively large overhead. Therefore, the maintenance of these
read sets and the validation later on should be outsourced to the hardware.
The hardware is equipped with \emph{mark bits} that allow the system to keep
track of the shared data access. We will illustrate this with an example.

The \emph{HASTM} system by Saha et al. has per-thread mark bits at cache-block
granularity. If a shared memory read is done, the hardware checks if the mark
bit for this address and thread is already set. If it is not set, the bit is
set and the memory address is added to the read set. When the transaction needs
to be validated, the STM can ask the hardware if any marked blocks have been
invalidated. The hardware has this information because it can monitor if a
marked cache block has been invalidated, meaning that it's possible (but not
given!) that another thread wrote to this memory region. Ideally, no marked
cache block has been invalidated and the STM can commit the transaction. If
there are invalidated blocks, the STM needs to fall back to (slow) software
validation by checking the version numbers of the data or it's locks, just like
a non-hardware-accelerated STM does. This software validation is needed because
of two reasons. First, a cache block can also be invalidated simply due to
limited cache capacity. Second, system events, such as interrupts and context
switches, can also provoke the eviction of cache lines. The mark bits are only
fast filters than are used to accelerate STM whenever possible.
% TODO: Maybe write something about SigTM too


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Hardware TM}

Hardware transactional memory is the system with both the most advantages and
the biggest drawbacks. A HTM system does not necessarily need any software
instrumentation and the entire transactional bookkeeping and instrumentation
can be implemented directly into hardware, which manages conflicts
transparently.  Programs just perform ordinary read and write access, and
programmers don't need to write special function bodies in their software, as
the system will be able to detect shared memory access even without special
annotations.  On top of that, it yields very good performance, with just 2\% to
10\% slower than non-transactional, serial code.  It up to 4 times faster than
classical lock-based synchronization and up to 2 times faster than
hardware-accelerated STM \cite{minh2007effective}.  Unfortunately, this comes
at the cost of portability. Only very few processors implement HTM, and there
is no clear specification on how transactional memory should be used.  As
stated above, it is still a research topic, and thus still unattractive for
production software that is not targeted at that specific hardware.

HTM systems make use of the cache hierarchy and the cache coherence protocol of
a computer in order to implement versioning and conflict detection. This is
possible because caches observe all reads and writes issued by a processor.
Just like hardware-accelerated STM, HTM systems use tracking bits to annotate
the read and write sets. We will again illustrate the behavior using an
example.

The \emph{Transactional Coherence and Consistency system} (TCC)
\cite{mcdonald2005characterization} is a deferred-update HTM and works as
follows. Cache blocks that are in the write set do not propagate their updates
until a transaction commits.  When the transaction tries to commit, the
coherence protocol is used to get exclusive access to all the cache blocks in
the write set. If this step succeeds, the transaction is valid and the
``write'' tracking bits are reset all at once, which will make the cache
coherence protocol propagate the changes, hereby effectively committing the
transaction and making it's results available to all the processors.  Notably,
this system allows for multiple transactions to commit in parallel.  It is, of
course, still possible that a conflict is detected when the transaction
attempts to commit. Namely, a conflict is detected when other caches receive a
coherence message, look up the address, and notice that read and/or write bits
are set for themselves. In this case, a software handler is called to abort and
potentially retry the local transaction.

There are a number of challenges in HTM systems that do not arise in STM
systems. At first, the caches have finite size and may overflow when larger
transactions are executed. Nevertheless, it is undesirable to limit
transactions to a certain size. Second, transactions may be interrupted by
interrupts, context switches or paging events, and the state needs to be
correctly restored afterwards. Both cases are handled by a system proposed by
Rajwar et al. named \emph{Virtualized TM} (VTM). VTM maps the bookkeeping data
to virtual memory, which has basically no size constraints and remains valid
across interrupts and context switches. Another possibility to cope with both
challenges is to implement a hybrid system which has both HTM and STM code
paths. If the hardware resources are exceeded, the systems falls back to
software handlers. As most transactions happen to be fairly small, the vast
majority is executed in a fast HTM-only mode, but the uncommon case of large
transactions can still be correctly handled in a software-only TM system.

From a hardware perspective, there are a number of things to consider. At
first, there is a need for new \texttt{load} and \texttt{store} instructions.
This brings advantages for the compiler, who can then distinguish between the
types of data access, namely if it is thread-local or shared memory, and
whether the data is mutable at all. This ability allows the compiler to produce
optimized code. We will shed some light into this by briefly examining the
\emph{Intel TSX} extension which is present on selected Haswell processors and
above \cite{wikipedia2015tsx}. The \emph{Transactional Synchronization
Extensions} implement an optimistic, cache-line-granularity, deferred-update
HTM system which detects conflicts through the cache coherence protocol. They
provide two different software interfaces.  The first one is \emph{Hardware
Lock Elision} (HLE). It consists of two new instructions that can be prefixed
to transactional code.  It is designed in such a way that instruction set
architectures without TSX simply ignore the instruction and fall back to
lock-based synchronization mechanisms, so backwards compatibility is preserved.
The principle of HLE is fairly simple: Imagine normal lock-based code. If a HLE
hint is found in the program, the processor does not actually take the lock,
i.e. the lock is \emph{elided}.  Instead, HTM mechanisms take place.  If a
conflict is found, the code block is re-executed, but this time, the lock is
actually taken. The second interface is called \emph{Restricted Transactional
Memory} (RTM). It is more powerful, as it gives the programmer the flexibility
to specify an alternative code path if the transaction fails.  It is, however,
not backwards compatible.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Current status of TM}

STM systems are already integrated in Haskell and Java. In the latter, it can
be used via the \texttt{atomic} keyword. In the Haskell community, it is the
preferred method for synchronization \cite{scott2015transactional}.
Furthermore, software TM is available in \texttt{gcc 4.7} and newer as a
compiler-specific language keyword extension for C and C++. It can be used via
the \texttt{\_\_transaction\_atomic} keyword. GCC is able to infer transaction
safety of functions automatically if they are defined in the same file,
otherwise further annotations are necessary \cite{marlier2012tm}. As of
\texttt{gcc 4.8} and above, it is also possible to specifically target the HLE
and RTM interfaces \cite{gcc0000rtm}\cite{gcc000hle}. For many other languages,
there are research-grade extensions and / or third-party libraries available
\cite{scott2015transactional}.

\begin{figure}[tbp]
  \begin{center}
    \resizebox{\linewidth}{!}{\input{benchmark_results.tex}}
    \caption{Benchmark results for $n = 1000000$ with 4 threads}
    \label{fig:benchmark}
  \end{center}
\end{figure}

We performed some benchmarks for C using the GCC language extensions. The
program performs a simple counter increment using a number of threads. There
are three different implementations. The first one uses no locks at all.  This
clearly produces wrong results, but it is useful as a ``base case'' to compare
against. The second implementation uses a classical \texttt{pthread\_mutex\_t}
lock to synchronize the incrementation of the common counter. The third
implementation uses the \texttt{\_\_transaction\_atomic} language extension.
The execution times are measured from the thread creation to the thread join in
each case using the \texttt{clock()} function from \texttt{time.h}. Note that
this function measures the effective CPU time and not the real time of the
program execution. The results can be seen in the ``Simple'' group in
\reffigure{benchmark}. It can be seen that the mutex-based version
outperforms the TM version by a factor of almost 2.

Next, we changed the three implementations to include a random waiting time
before executing the counter increment. Note that the sleeping is done outside
of the synchronized blocks if there are any. Looking at
\reffigure{benchmark} again, where the results are plotted in the
``Interval'' group, we can now see that the mutex-based version still
outperforms the TM version, but by a larger margin. The timing measurement of
the unsynchronized version shows that we are indeed measuring a huge amount of
random number generation, but nevertheless, the mutex-based version clearly
dominates. Note that we are \emph{not} measuring the actual sleeping time,
thanks to the \texttt{clock()} function.  This result was to be expected: The
introduction of a waiting period made the implementations produce less
conflicts, which is favorable for both versions. We assume that the two main
disadvantages of the TM version are a comparatively large bookkeeping overhead
for a tiny operation (\texttt{counter++}) on the one hand, and the fact that
the mutex-based version could make use of the fast communication because it has
been run on one hyper-threaded quad-core CPU on the other hand. Nevertheless,
judging by how much time the repeated calls to \texttt{urand()} added to our
benchmark (which is generally not a bottleneck for applications), we conclude
that the observed differences between mutex-based and TM-based synchronization
are small enough to justify the usage of TM in a program without slowing it
down considerably.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Outlook \& conclusion}

With the rise of multi-core processors, the need for program parallelization calls
for simple yet well-performing synchronization mechanisms. Transactional memory
is a promising extension of today's possibilities, and although it is very
unlikely that it will replace classic lock-based orchestration entirely
\cite{blundell2006subtleties}, there is a clear interest in the topic. It has
matured to production-ready (although probably not mission-critical) systems
over the years and still continues to evolve.  TM-specific instructions have
become a permanent addition to the X86 instruction set and language support is
being developed for a multitude of programming languages.  There are still some
challenges that need to be taken, the most important one probably being the
standardization of the semantics of a TM interface that should ideally support
both HTM and STM back-ends.  Furthermore, some further research is needed to
determine the best implementation variants, e.g. if a conflict in HTM systems
should be handled in HTM itself or if STM routines should be invoked. We got
the impression that transactional memory will see a rise in the software
industry and see widespread adoption in the years to come. We conclude that the
ideas as well as the implementations of TM look very promising and that today's
programmers should have a look at this topic that they are likely to use more
often in the near future.

%\appendix
\balance \printbibliography

