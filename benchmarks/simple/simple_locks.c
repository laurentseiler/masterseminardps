#include "../common.h"

unsigned long count = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void *do_something(void *arg)
{
	for (int i = 0; i < COUNT_TO; i++)
	{
		pthread_mutex_lock(&mutex);
		count++;
		pthread_mutex_unlock(&mutex);
	}

	return NULL;
}

int main(int argc, char **argv)
{
	if (argc != 2 || atoi(argv[1]) <= 0)
		fail("Please specify the number of threads!");

	int num_threads = atoi(argv[1]);
	pthread_t tid[num_threads];
	clock_t start = clock();
	double duration;

	for (int i = 0; i < num_threads; i++)
	{
		if (pthread_create(tid + i, NULL, &do_something, NULL) != 0)
			fail("Error creating thread!");
	}

	for (int i = 0; i < num_threads; i++)
	{
		if (pthread_join(tid[i], NULL) != 0)
			fail("Error joining thread!");
	}

	duration = (clock() - start * 1.0) / CLOCKS_PER_SEC;

	printf("Done! Final result: %lu (%f seconds)\n", count, duration);

	return EXIT_SUCCESS;
}
