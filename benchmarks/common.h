#ifndef COMMON_H
#define COMMON_H

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

#define fail(msg) { \
	fprintf(stderr, "%s\n", (msg)); \
	exit(EXIT_FAILURE); }

#define sleep_init() srand(pthread_self());

#define sleep_do() { \
	struct timespec sleeptime = {0, (rand() * 1.0) / 1000}; \
	nanosleep(&sleeptime, NULL); }

#define COUNT_TO 1000000

#endif // COMMON_H
