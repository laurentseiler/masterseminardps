#!/bin/bash

TMPFILE=$(mktemp)

echo "Test NoLocks Mutex TM" > $TMPFILE

cd simple
tmp="$(make clean run)"
tmp="$(echo "$tmp" | grep seconds | sed 's/.*(\(.*\) seconds)/\1/' | tr '\n' ' ')"
tmp="$(echo "Simple $tmp" )"
echo "$tmp" >> $TMPFILE

cd -

cd intervals
tmp="$(make clean run)"
tmp="$(echo "$tmp" | grep seconds | sed 's/.*(\(.*\) seconds)/\1/' | tr '\n' ' ')"
tmp="$(echo "Intervals $tmp" )"
echo "$tmp" >> $TMPFILE

cd -

gnuplot <<EOF
# set terminal x11 persist
set terminal epslatex monochrome
set output 'benchmark_results.tex'
# set style fill solid border
set style data histogram
set style histogram clustered
set ylabel "Effective computation time in seconds"
plot for [COL=2:4] "$TMPFILE" using COL:xticlabels(1) title columnheader
set t png
set output
EOF

